<?php
require_once("../../../vendor/autoload.php");

$objBookTitle = new \App\ProfilPicture\ProfilPicture();
$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view();

if(isset($_GET['YesButton'])) $objBookTitle->delete();

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture - Single Person Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
            background-color:#1b6d85;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
        body{
            background: url("profile2.jpg") no-repeat;
            background-size: 100%;

        }
        h1{
            color: #1b6d85;
        }
    </style>



</head>
<body>

<br><br><br><br>
<div class="container">
    <h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Name</th>
            <th>Picture</th>
        </tr>

        <?php

            echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td><img src='Upload/$oneData->image' width='50px'></td>

                  </tr>
              ";

        ?>

    </table>

<?php
   echo "
          <a href='delete.php?id=$oneData->id&YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='index.php' class='btn btn-success'>No</a>
        ";

?>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>