<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use \App\BookTitle\BookTitle;

if(isset($_POST['mark'])) {

    $email = new \App\Email\Email();
    $email->recoverMultiple($_POST['mark']);

    Utility::redirect("ViewAllData.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("viewAllsoftDeletedFromEmail.php?page=1");

}