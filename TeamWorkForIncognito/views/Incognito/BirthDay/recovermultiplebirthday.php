<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use \App\BirthDay\BirthDay;

if(isset($_POST['mark'])) {

    $objBirthDay = new BirthDay();
    $objBirthDay->recoverMultiple($_POST['mark']);

    Utility::redirect("indexbirthday.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("trashedbirthday.php");

}