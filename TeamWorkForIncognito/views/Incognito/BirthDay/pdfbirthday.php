<?php
include_once ('../../../vendor/autoload.php');
use App\BirthDay\BirthDay;

$obj= new BirthDay();
 $recordSet=$obj->index();
 //var_dump($allData);
$trs="";
$sl=0;

    foreach($recordSet as $row) {
        $id =  $row->id;
        $persiName = $row->p_name;
        $dob =$row->dob;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50'> $sl</td>";
        $trs .= "<td width='50'> $id </td>";
        $trs .= "<td width='250'> $personName </td>";
        $trs .= "<td width='250'> $dob </td>";

        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Person Name</th>
                    <th align='left' >Birth Day</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('Birthdaylist.pdf', 'D');