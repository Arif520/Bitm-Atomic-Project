<?php
/**
 * Created by PhpStorm.
 * User: abcd
 * Date: 04-02-2017
 * Time: PM 12.37
 */

namespace App\Email;


use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use MongoDB\Driver\Manager;
use PDO;

class Email extends Database
{
    private $id;
    private $name;
    private $email;
    private $isSoftDeleted;

    public function setData($arrayData){
        if(array_key_exists("id",$arrayData))
            $this->id = $arrayData["id"];
        if(array_key_exists("personName",$arrayData))
            $this->name = $arrayData["personName"];
        if(array_key_exists("personEmai",$arrayData))
            $this->email = $arrayData["personEmai"];
        if(array_key_exists("isSoftDeleted",$arrayData))
            $this->isSoftDeleted = $arrayData["isSoftDeleted"];
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function storeToDatabase(){
        $dataArray = array($this->name,$this->email);
        $SQL = "Insert into email_table(name,email) VALUES (?,?)";
        $statementHandler = $this->DBH->prepare($SQL);
        $result = $statementHandler->execute($dataArray);
        if($result){
           Message::message("Success! Data Inserted");
        }
        else{
            Message::message("Failed! Data Not Inserted");
        }
        Utility::redirect("ViewAllData.php");
    }

    public function getAllDataRecords(){
        $SQL = "Select * from email_table WHERE isSoftDeleted='No'";
        $statementHandler = $this->DBH->query($SQL);
        $statementHandler->setFetchMode(PDO::FETCH_OBJ);
        return $statementHandler->fetchAll();
    }

    public function getSingleDataRecords(){
        $SQL = "Select * from email_table WHERE id=".$this->id;
        $statementHandler = $this->DBH->query($SQL);
        $statementHandler->setFetchMode(PDO::FETCH_OBJ);
        return $statementHandler->fetch();
    }

    public function editRecords(){
        $dataArray = array($this->name,$this->email);
        $SQL = "UPDATE email_table SET name=?, email=? WHERE id=".$this->id;
        $statehemthandler = $this->DBH->prepare($SQL);
        $result = $statehemthandler->execute($dataArray);
        if($result)
            Message::message("Data Updated");
        else
            Message::message("Update Failed");

        Utility::redirect("ViewAllData.php");
    }

    public function softDeleted(){
        $dataArray = array("Yes");
        $SQL = "UPDATE email_table SET isSoftDeleted=? WHERE id=".$this->id;
        $statehemthandler = $this->DBH->prepare($SQL);
        $result = $statehemthandler->execute($dataArray);
        if($result)
            Message::message("Data Soft Deleted");
        else
            Message::message("Soft Deleted Failed");

        Utility::redirect("ViewAllData.php");
    }

    public function getAllsofeDeletedDataRecords(){
        $SQL = "Select * from email_table WHERE isSoftDeleted='Yes'";
        $statementHandler = $this->DBH->query($SQL);
        $statementHandler->setFetchMode(PDO::FETCH_OBJ);
        return $statementHandler->fetchAll();
    }

    public function recovery(){
        $dataArray = array("No");
        $SQL = "UPDATE email_table SET isSoftDeleted=? WHERE id=".$this->id;
        $statehemthandler = $this->DBH->prepare($SQL);
        $result = $statehemthandler->execute($dataArray);
        if($result)
            Message::message("Data Recovered");
        else
            Message::message("Soft Recovered Failed");

        Utility::redirect("ViewAllSoftDeletedData.php");
    }

    public function recoverMultiple($markArray){

        foreach($markArray as $id){

            $sql = "UPDATE  email_table SET isSoftDeleted='No' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }

        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");

        Utility::redirect("ViewAllData.php?Page=1");
    }

    public function delete(){
        $SQL = "DELETE from email_table WHERE id=".$this->id;
        $result = $this->DBH->exec($SQL);
        if($result)
            Message::message("Recorde Deleted");
        else
            Message::message("Failed, Records Not Deleted");

        Utility::redirect("ViewAllHobbyData.php");
    }

    public function viewAllDataPaginator($page=1,$itemsPerPage=3){
        $start = (($page-1) * $itemsPerPage);
        $sql = "SELECT * from email_table  WHERE isSoftDeleted = 'No' LIMIT $start,$itemsPerPage";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

    public function viewAllSoftDeletedDataPaginator($page=1,$itemsPerPage=3){
        $start = (($page-1) * $itemsPerPage);
        $sql = "SELECT * from email_table  WHERE isSoftDeleted = 'Yes' LIMIT $start,$itemsPerPage";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }

    public function search($requestArray){
        $sql = "";

        if( isset($requestArray['byName']) && isset($requestArray['byEmail']) )  $sql = "SELECT * FROM `email_table` WHERE `isSoftDeleted` ='No' AND (`name` LIKE '%".$requestArray['search']."%' OR `email` LIKE '%".$requestArray['search']."%')";

        if(isset($requestArray['byName']) && !isset($requestArray['byEmail']) ) $sql = "SELECT * FROM `email_table` WHERE `isSoftDeleted` ='No' AND `name` LIKE '%".$requestArray['search']."%'";

        if(!isset($requestArray['byName']) && isset($requestArray['byEmail']) )  $sql = "SELECT * FROM `email_table` WHERE `isSoftDeleted` ='No' AND `email` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;
    }// end of search()

    public function getAllKeywords(){
        $_allKeywords = array();
        //$WordsArr = array();

        $allData = $this->getAllDataRecords();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }
        // $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each  Name search field block end

        // for each Email search field block start
        $allData = $this->getAllDataRecords();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->email);
        }
        $allData = $this->getAllDataRecords();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->email);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each Email search field block end

        return array_unique($_allKeywords);

    }// get all keywords

    public function multipleSelectedItemTrashed($selectedIDsArray){
        foreach($selectedIDsArray as $id){
            $sql = "UPDATE  email_table SET isSoftDeleted='Yes' WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }

        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");

        Utility::redirect('ViewAllSoftDeletedData.php?Page=1');
    }

    public function deleteMultiple($selectedIDsArray){
        foreach($selectedIDsArray as $id){
            $sql = "Delete from email_table  WHERE id=".$id;
            $result = $this->DBH->exec($sql);
            if(!$result) break;
        }
        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");
        Utility::redirect('ViewAlldata.php?Page=1');
    }

    public function listSelectedData($selectedIDs){
        foreach($selectedIDs as $id){
            $sql = "Select * from email_table  WHERE id=".$id;
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            $someData[]  = $STH->fetch();
        }
        return $someData;
    }

}