<?php
require_once("../../../vendor/autoload.php");

$obj= new App\SummaryOfCompany\Summary();
$obj->setData($_GET);
$oneData = $obj->viewDt();


?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Summery of Organization - Single Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }


         body{
             background: url("organization1.jpg") no-repeat;
             background-size: 100%;

         }

        h1{
            color: rebeccapurple;
        }

    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">View Single Data</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Name</th>
            <th>Summary</th>
            <th>Action Buttons</th>
        </tr>

        <?php

            echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->summary</td>

                     <td><a href='indexData.php' class='btn btn-info'>Back To Active List</a> </td>
                  </tr>
              ";

        ?>

    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>