<?php
require_once("../../../vendor/autoload.php");

$hobby = new \App\Hobby\Hobbies();
$hobby->setData($_GET);
$oneData = $hobby->getSingleData();
$arrayHobby = explode(",",$oneData->hobby);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Single Hobby Data</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
        body{
            background: url("hobby1.jpg") no-repeat;
            background-size: 100%;

        }

        .information{

            background-color:forestgreen;
            color: #fff;
            font-weight: bold;
            padding: 10px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            align-content: center;
            align-items: center;
            alignment: center;

            border: solid;


        }

        .main{
            align-content: center;
            align-items: center;
            alignment: center;
            width:500px;
            height: 300px;
            display: inline-block;


        }

        h1{
            color: white;
        }

    </style>



</head>
<body>



<body>

<div class="container">

    <div class="navbar">
        <td><a href='../../../../index.html' class='btn btn-group-lg btn-info'>Home</a> </td>
        <td><a href='ViewAllHobbyData.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>

    <center>
        <div class="main">





            <h1>Hobby Edit</h1><br>
            <div class="information">
    <form class="form-group" action="hobbyUpdateFromEditFile.php" method="post">

        Name:
        <input class="form-control" type="text" name="name" value="<?php echo $hobbies->name ?>">
        <br>
        Chose Your Hobies <br>
        <input type="checkbox" <?php if(in_array("Progrmming",$hobbies)) echo "checked" ?> name = mark[] value="Progrmming">Progrmming
        <br>
        <input type="checkbox"  <?php if(in_array("Swming",$hobbies)) echo "checked" ?> name = mark[] value="Swming">Swming
        <br>
        <input type="checkbox"  <?php if(in_array("Rideing",$hobbies)) echo "checked" ?> name = mark[] value="Rideing">Rideing
        <br>
        <input type="checkbox"  <?php if(in_array("Football",$hobbies)) echo "checked" ?> name = mark[] value="Football">Football
        <br>
        <input type="checkbox"  <?php if(in_array("Cycling",$hobbies)) echo "checked" ?> name = mark[] value="Cycling">Cycling
        <br>
        <input type="checkbox"  <?php if(in_array("Drawing",$hobbies)) echo "checked" ?> name = mark[] value="Drawing">Drawing
        <br><br>
        <input type="hidden"  name="id" value="<?php echo $hobbies->id?>">
        <input type="submit">

    </form>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>