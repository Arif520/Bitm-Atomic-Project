<?php
require_once("../../../vendor/autoload.php");

use \App\BookTitle\BookTitle;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])) {

$Hobby= new \App\Hobby\Hobbies();

$Hobby->multipleSelectedItemTrashed($_POST['mark']);
    Utility::redirect("ViewAllSoftDeletedData.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("ViewAllHobbyData.php");
}