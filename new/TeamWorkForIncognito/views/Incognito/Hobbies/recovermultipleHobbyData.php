<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use \App\BookTitle\BookTitle;

if(isset($_POST['mark'])) {

    $hobby = new \App\Hobby\Hobbies();
    $hobby->recoverMultiple($_POST['mark']);

    Utility::redirect("ViewAllHobbyData.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("viewAllsoftDeletedData.php?page=1");
}