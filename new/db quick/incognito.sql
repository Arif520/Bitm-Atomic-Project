-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2017 at 10:38 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `incognito`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_city`
--

CREATE TABLE IF NOT EXISTS `add_city` (
`id` int(11) NOT NULL,
  `city_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `add_gender`
--

CREATE TABLE IF NOT EXISTS `add_gender` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_gender`
--

INSERT INTO `add_gender` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'new ziad', 'female', 'No'),
(2, 'aSADA', 'male', 'No'),
(3, 'saSASDASDASD', 'male', 'No'),
(4, 'rifat', 'other', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE IF NOT EXISTS `birth_day` (
`id` int(11) NOT NULL,
  `p_name` varchar(111) NOT NULL,
  `dob` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `p_name`, `dob`, `soft_deleted`) VALUES
(1, 'adsdasd', '2017-02-06', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_table`
--

CREATE TABLE IF NOT EXISTS `email_table` (
`id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf32_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf32_unicode_ci NOT NULL,
  `isSoftDeleted` varchar(11) COLLATE utf32_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `email_table`
--

INSERT INTO `email_table` (`id`, `name`, `email`, `isSoftDeleted`) VALUES
(1, 'Rifat', 'rifatcse17@gmail.com', 'Yes'),
(3, 'Maruf', 'sdfsfdsf', 'No'),
(4, 'Shawon', 'shawon@email.com', 'No'),
(7, 'Rabbi', 'Rabbi@yahoo.com', 'Yes'),
(13, 'Rifat Islam', 'rifatcse17@gmail.com', 'No'),
(14, 'Name', 'mail@mail.com', 'Yes'),
(16, 'Rifat Islam', 'r@i.com', 'Yes'),
(17, 'Rifat Islam', 't@y.com', 'Yes'),
(19, 'fdfd', 'fdfd@jscnams.com', 'Yes'),
(20, 'fdfd', 'fdfd@jscnams.com', 'Yes'),
(21, 'asad', 'asad@gmail.com', 'No'),
(22, 'asad', 'asad@gmail.com', 'No'),
(23, 'asad', 'asad@gmail.com', 'No'),
(24, 'asad', 'asad@gmail.com', 'No'),
(25, 'dsfdsf', 'sdddddd@yahoo.com', 'No'),
(26, 'asasdsad', 'asad@gmail.com', 'No'),
(27, 'asasdsad', 'asad@gmail.com', 'Yes'),
(28, 'asasdsad', 'asad@gmail.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobby_table`
--

CREATE TABLE IF NOT EXISTS `hobby_table` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `hobby` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby_table`
--

INSERT INTO `hobby_table` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'Rifat Islam', 'Progrmming,Swming,Rideing,Football,Cycling', 'No'),
(2, 'Asad', 'Nai', 'Yes'),
(3, 'rakib bhai', 'Swming,Cycling', 'No'),
(4, 'adsdasd', 'Rideing,Drawing', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profil_picture`
--

CREATE TABLE IF NOT EXISTS `profil_picture` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `image` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profil_picture`
--

INSERT INTO `profil_picture` (`id`, `name`, `image`, `soft_deleted`) VALUES
(3, 'group', '1487233616_group.jpg', 'No'),
(10, 'asfdasdada', '1486891783img_tree.gif', 'No'),
(11, 'asasasa', '1486891912klematis.jpg', 'No'),
(12, 'sir', '1487233593bitmsir.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE IF NOT EXISTS `summary_of_organization` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `summary` text NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `name`, `summary`, `soft_deleted`) VALUES
(1, 'asdsafdsad', 'asdasdadssss', 'No'),
(2, 'new name', '            asdsad\r\nasd', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_city`
--
ALTER TABLE `add_city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_gender`
--
ALTER TABLE `add_gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_table`
--
ALTER TABLE `email_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby_table`
--
ALTER TABLE `hobby_table`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil_picture`
--
ALTER TABLE `profil_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_city`
--
ALTER TABLE `add_city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `add_gender`
--
ALTER TABLE `add_gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_table`
--
ALTER TABLE `email_table`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `hobby_table`
--
ALTER TABLE `hobby_table`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `profil_picture`
--
ALTER TABLE `profil_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
